<?php
	include('connection.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SICenayang - Help You to Prepare Your Future</title>

    <meta name="keywords" content="">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,500,700,800' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Css animations  -->
    <link href="css/animate.css" rel="stylesheet">

    <!-- Theme stylesheet, if possible do not edit this stylesheet -->
    <link href="css/style.default.css" rel="stylesheet" id="theme-stylesheet">

    <!-- Custom stylesheet - for your changes -->
    <link href="css/custom.css" rel="stylesheet">
	
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="img/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="img/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="img/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="img/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="img/apple-touch-icon-152x152.png" />

</head>

<body>
		<div id="heading-breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h1>LAMAN PENGISIAN DATA DIRI</h1>
                    </div>
                    <div class="col-md-5">
                        <ul class="breadcrumb">
                            <li><a href="index.html">Home</a>
                            </li>
                            <li>LAMAN PENGISIAN DATA DIRI</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div id="content">
            <div class="container" id="contact">
               <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h2 class="text-uppercase">isi data diri</h2>
                            <p class="lead">Ingin menyusun jadwal terbaik selama kuliah?</p>
                            <p>Ingin menyusun jadwal kuliah terbaik selama di Fasilkom? Ingin juga mencapai segala target dengan persiapan jadwal yang matang?</p>
                            <p class="text-muted">If you have any questions, please feel free to <a href="contact.php">contact us</a>, our customer service center is working for you 24/7.</p>

                            <hr>
                            <form action="contact.php" method="post">
								<input type="hidden" name="command" value="insertDataDiri">
                                <div class="form-group">
                                    <label for="name-regis">Nama</label>
                                    <input type="text" required="required" name="name-regis" title="Nama harus diisi!" placeholder="Masukkan nama panjang anda" class="form-control" id="name-regis">
                                </div>
                                 <div class="form-group">
                                    <label for="NPM-regis">NPM</label>
                                    <input type="text" required="required" name="NPM-regis" title="NPM harus diisi!" pattern="\d{10}" placeholder="Masukkan NPM anda" class="form-control" id="NPM-regis">
                                </div>
								<div class="form-group">
                                    <label for="semester-regis">Semester</label>
                                    <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                                      <select class="form-control" name="semester-regis">
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
											<option value="6">6</option>
											<option value="7">7</option>
											<option value="8">8</option>
                                      </select>
                                </div>
								<div class="form-group">
                                    <label for="address">Index Prestasi Terakhir (IPT)</label>
                                    <input type="text" required="required" name="IPT" title="IPT harus berupa angka dengan titik" placeholder="Masukkan IPK anda dengan pemisah titik (ex: 3.0)" class="form-control" id="address">
                                </div>
								<div class="form-group">
                                    <label for="address">Index Prestasi Kumulatif (IPK)</label>
                                    <input type="text" required="required" name="IPK" title="IPK harus berupa angka dengan titik" placeholder="Masukkan IPK anda dengan pemisah titik (ex: 3.0)" class="form-control" id="address">
                                </div>
								<div class="form-group">
                                    <label for="address">Total Jumlah SKS Lulus</label>
                                    <input type="text" required="required" name="SKS" title="IPK harus berupa angka dengan titik" placeholder="Masukkan Jumlah Total SKS anda yang sudah lulus (ex: 44)" class="form-control" id="address">
                                </div>
								<div class="form-group">
                                    <label for="gender-login">Target lulus dalam .. semester</label>
                                    <span class="input-group-addon"><i class="fa fa-calendar fa" aria-hidden="true"></i></span>
                                      <select class="form-control" name="target-lulus">
                                        <option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
                                      </select>
                                </div>
								<div class="form-group">
                                    <label for="address">IPK yang diinginkan :</label>
                                    <input type="text" required="required" name="IPK-want" title="IPK harus berupa angka dengan titik" placeholder="Masukkan Jumlah Total SKS anda yang sudah lulus (ex: 44)" class="form-control" id="address">
                                </div>
                                <div class="text-center">
									<input type="hidden" name="command" value="insertDataDiri">
                                    <button type="submit" class="btn btn-template-main"><i class="fa fa-user-md"></i> Register</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#content -->

        <section class="bar background-pentagon no-mb">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading text-center">
                            <h2>MEET THE TEAM</h2>
                        </div>

                        <p class="lead text-center">Kami sangat senang membantu menyiapkan masa depan kalian karena ini pun menentukan masa depan kami! Doakan kami lulus Matkul SC dengan baik!</p>


                        <!-- *** TESTIMONIALS CAROUSEL ***
 _________________________________________________________ -->

                       <div class="row">
						<div class="col-md-1 col-sm-3">
						</div>
                        <div class="col-md-3 col-sm-3">
                            <div class="team-member" data-animate="fadeInDown">
                                <div class="image">
                                    <a href="#">
                                        <img src="image/ariz.jpg" alt="" class="img-responsive img-circle">
                                    </a>
                                </div>
                                <h3><a href="#">ARIZ BUDITIONO</a></h3>
                                <p class="role">Mahasiswa</p>
                            </div>
                            <!-- /.team-member -->
                        </div>
                        
                        <div class="col-md-3 col-sm-3" data-animate="fadeInDown">
                            <div class="team-member">
                                <div class="image">
                                    <a href="#">
                                        <img src="image/mega.jpg" alt="" class="img-responsive img-circle">
                                    </a>
                                </div>
                                <h3><a href="#">MEGA MUTIARA</a></h3>
                                <p class="role">Mahasiswi</p>
                            </div>
                            <!-- /.team-member -->
                        </div>
                        <div class="col-md-3 col-sm-3" data-animate="fadeInDown">
                            <div class="team-member">
                                <div class="image">
                                    <a href="#">
                                        <img src="image/person-8.jpg" alt="" class="img-responsive img-circle">
                                    </a>
                                </div>
                                <h3><a href="#">ZAHRA A. UMMAH</a></h3>
                                <p class="role">Mahasiswi</p>
                            </div>
                            <!-- /.team-member -->
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <!-- *** GET IT ***
_________________________________________________________ -->

        <div id="get-it">
            <div class="container">
                <div class="col-md-8 col-sm-12">
                    <h3>MAKE YOU EASIER TO PREPARE YOUR FUTURE!</h3>
                </div>
                <div class="col-md-4 col-sm-12">
                    <a href="#" class="btn btn-template-transparent-primary">LET'S Prepare</a>
                </div>
            </div>
        </div>


        <!-- *** GET IT END *** -->


        <!-- *** COPYRIGHT ***
_________________________________________________________ -->

        <div id="copyright">
            <div class="container">
                <div class="col-md-12">
                    <p class="pull-left">&copy; 2017. Fakultas Ilmu Komputer / ZAM ZAM Cerdas</p>
                    <p class="pull-right">Template by <a href="https://bootstrapious.com">Bootstrapious</a> & <a href="https://remoteplease.com">Remote Please</a>
                         <!-- Not removing these links is part of the license conditions of the template. Thanks for understanding :) If you want to use the template without the attribution links, you can do so after supporting further themes development at https://bootstrapious.com/donate  -->
                    </p>

                </div>
            </div>
        </div>
        <!-- /#copyright -->

        <!-- *** COPYRIGHT END *** -->



    </div>
    <!-- /#all -->

    <!-- #### JAVASCRIPT FILES ### -->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')
    </script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

    <script src="js/jquery.cookie.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/jquery.parallax-1.1.3.js"></script>
    <script src="js/front.js"></script>

    

    <!-- gmaps -->

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>

    <script src="js/gmaps.js"></script>
    <script src="js/gmaps.init.js"></script>

    <!-- gmaps end -->





</body>

</html>