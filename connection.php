<?php
function connectDB(){
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "sicenayang";

	// Create connection
	$conn = mysqli_connect($servername, $username, $password, $dbname);

	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	} 
	return $conn;
}

function jsonData() {
	$nama = $_POST['name-regis'];
	$NPM = $_POST['NPM-regis'];
	$semester = $_POST['semester-regis'];
	$IPT = $_POST['IPT'];
	$IPK = $_POST['IPK'];
	$SKS = $_POST['SKS'];
	$TargetSemester = $_POST['target-lulus'];
	$IPKwant = $_POST['IPK-want'];
	
	$file = "InputData.json";
	
	$json = json_decode(file_get_contents($file), true);
	$json[$nama] = array("NPM" => $NPM, "semester" => $semester, "IPT" => $IPT, "IPK" => $IPK, "SKS" => $SKS, "TargetSemester" => $TargetSemester, "IPKwant" => $IPKwant);
	file_put_contents($file, json_encode($json));
}

function jsonMatkul(){
		$namaMatkul = $_POST['nama-matkul']; //berupa nama matkul
		$namaSemester = $_POST['nama-semester']; //angka semester
		
		$file = "InputMatkul.json";
		for($i=0; $i<sizeof($namaMatkul); $i++){
			$json = json_decode(file_get_contents($file), true);
			$json[] = array("Nama Matkul" => $namaMatkul[$i], "Semester" => $namaSemester[$i]);
			file_put_contents($file, json_encode($json));
		}
		
}

if (isset($_POST['insertMatkul'])) {
		jsonMatkul();
}
if($_SERVER['REQUEST_METHOD'] === 'POST') {

	if($_POST['command'] === 'insertDataDiri'){
		jsonData();
	}
}
?>
