<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta name="robots" content="all,follow">
		<meta name="googlebot" content="index,follow,snippet,archive">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>SICenayang - Help You to Prepare Your Future</title>
		<meta name="keywords" content="">
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,500,700,800' rel='stylesheet' type='text/css'>
		<!-- Bootstrap and Font Awesome css -->
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

		<!-- Css animations  -->
		<link href="css/animate.css" rel="stylesheet">

		<!-- Theme stylesheet, if possible do not edit this stylesheet -->
		<link href="css/style.default.css" rel="stylesheet" id="theme-stylesheet">

		<!-- Custom stylesheet - for your changes -->
		<link href="css/custom.css" rel="stylesheet">
		<!-- Favicon and apple touch icons-->
		<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="img/apple-touch-icon.png" />
		<link rel="apple-touch-icon" sizes="57x57" href="img/apple-touch-icon-57x57.png" />
		<link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png" />
		<link rel="apple-touch-icon" sizes="76x76" href="img/apple-touch-icon-76x76.png" />
		<link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png" />
		<link rel="apple-touch-icon" sizes="120x120" href="img/apple-touch-icon-120x120.png" />
		<link rel="apple-touch-icon" sizes="144x144" href="img/apple-touch-icon-144x144.png" />
		<link rel="apple-touch-icon" sizes="152x152" href="img/apple-touch-icon-152x152.png" />
		<link href="css/owl.carousel.css" rel="stylesheet">
		<link href="css/owl.theme.css" rel="stylesheet">
	</head>

<body>
    <div id="all">
        <header>
            <div id="top">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-5 contact">
                            <p class="hidden-sm hidden-xs">Contact us on +6287882757687 or SICenayang@ui.com.</p>
                            <p class="hidden-md hidden-lg"><a href="#" data-animate-hover="pulse"><i class="fa fa-phone"></i></a>  <a href="#" data-animate-hover="pulse"><i class="fa fa-envelope"></i></a>
                            </p>
                        </div>
                        <div class="col-xs-7">
                            <div class="social">
                                <a href="#" class="external facebook" data-animate-hover="pulse"><i class="fa fa-facebook"></i></a>
                                <a href="#" class="external gplus" data-animate-hover="pulse"><i class="fa fa-google-plus"></i></a>
                                <a href="#" class="external twitter" data-animate-hover="pulse"><i class="fa fa-twitter"></i></a>
                                <a href="#" class="email" data-animate-hover="pulse"><i class="fa fa-envelope"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="navbar-affixed-top" data-spy="affix" data-offset-top="200">
                <div class="navbar navbar-default yamm" role="navigation" id="navbar">

                    <div class="container">
                        <div class="navbar-header">

                            <a class="navbar-brand home" href="index.html">
                                <img src="image/logo-small.png" alt="Universal logo" class="hidden-xs hidden-sm">
                                <img src="image/logo-small.png" alt="Universal logo" class="visible-xs visible-sm"><span class="sr-only">Universal - go to homepage</span>
                            </a>
                            <div class="navbar-buttons">
                                <button type="button" class="navbar-toggle btn-template-main" data-toggle="collapse" data-target="#navigation">
                                    <span class="sr-only">Toggle navigation</span>
                                    <i class="fa fa-align-justify"></i>
                                </button>
                            </div>
                        </div>
                        <div class="collapse clearfix" id="search">
                            <form class="navbar-form" role="search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search">
                                    <span class="input-group-btn">
										<button type="submit" class="btn btn-template-main"><i class="fa fa-search"></i></button>
									</span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="Login">Customer login</h4>
                    </div>
                    <div class="modal-body">
                        <form action="customer-orders.html" method="post">
                            <div class="form-group">
                                <input type="text" class="form-control" id="email_modal" placeholder="email">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="password_modal" placeholder="password">
                            </div>
                            <p class="text-center">
                                <button class="btn btn-template-main"><i class="fa fa-sign-in"></i> Log in</button>
                            </p>
                        </form>
                        <p class="text-center text-muted">Not registered yet?</p>
                        <p class="text-center text-muted"><a href="customer-register.html"><strong>Register now</strong></a>! It is easy and done in 1&nbsp;minute and gives you access to special discounts and much more!</p>
                    </div>
                </div>
            </div>
        </div>

        <div id="content">
            <div class="container">
                <div class="col-sm-6 col-sm-offset-3" id="error-page">
                    <div class="box">
                        <p class="text-center">
                            <a href="index.html">
                                <img src="image/logo-small.png">
                            </a>
                        </p>
                        <h3>Make You Easier to Prepare Your Future!</h3>
                        <h4 class="text-muted">SICenayang - Sistem Cerdas</h4>
                        <p class="buttons"><a href="index2.html" class="btn btn-template-main"><i class="fa fa-home"></i>REGISTER</a></p>
                    </div>
                </div>
            </div>
        </div>

        <div id="get-it">
            <div class="container">
                <div class="col-md-8 col-sm-12">
                    <h3>MAKE YOU EASIER TO PREPARE YOUR FUTURE!</h3>
                </div>
                <div class="col-md-4 col-sm-12">
                    <a href="#" class="btn btn-template-transparent-primary">LET'S Prepare</a>
                </div>
            </div>
        </div>

		<!---FOOTER -->
        <footer id="footer">
            <div class="container">
                <div class="col-md-3 col-sm-6">
                    <h4>About us</h4>

                    <p>We try to help you all to prepare the best for your future</p>

                    <hr>

                    <h4>Join our monthly newsletter</h4>

                    <form>
                        <div class="input-group">
                            <input type="text" class="form-control">
                            <span class="input-group-btn">
								<button class="btn btn-default" type="button"><i class="fa fa-send"></i></button>
							</span>
                        </div>
                    </form>
                    <hr class="hidden-md hidden-lg hidden-sm">
                </div>
				
                <div class="col-md-3 col-sm-6">
                    <h4>Contact</h4>
                    <p><strong>ZAM-ZAM Cerdas</strong>
                        <br>Fakultas Ilmu Komputer
                        <br>Universitas Indonesia
                        <br>Depok
                        <br>Indonesia
						<br>
                        <strong>SICenayang </strong>
                    </p>
                    <a href="" class="btn btn-small btn-template-main">Go to contact page</a>
                    <hr class="hidden-md hidden-lg hidden-sm">
                </div>
			
				<div class="col-md-3 col-sm-6">
					<ul class="owl-carousel customers">
                        <li class="item">
                            <img src="image/f1.png" alt="" class="img-responsive">
                        </li>
                    </ul>
                </div>
				<div class="col-md-3 col-sm-6">
					<ul class="owl-carousel customers">
						<li class="item">
                            <img src="image/f3.png" alt="" class="img-responsive">
                        </li>
                    </ul>
                </div>
            </div>
        </footer>

        <div id="copyright">
            <div class="container">
                <div class="col-md-12">
                    <p class="pull-left">&copy; 2017. ZAMZAM Cerdas / SICenayang</p>
                    <p class="pull-right">Template by <a href="https://bootstrapious.com">Bootstrapious</a> & <a href="https://remoteplease.com">Remote Please</a>
                         <!-- Not removing these links is part of the license conditions of the template. Thanks for understanding :) If you want to use the template without the attribution links, you can do so after supporting further themes development at https://bootstrapious.com/donate  -->
                    </p>
                </div>
            </div>
        </div>
    </div>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	
    <script>
        window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')
    </script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

    <script src="js/jquery.cookie.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/jquery.parallax-1.1.3.js"></script>
    <script src="js/front.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
	<script src="js/gmaps.js"></script>
    <script src="js/gmaps.init.js"></script>
</body>

</html>