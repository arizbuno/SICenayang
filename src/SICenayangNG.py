import heapq


class Matkul(object):
    """docstring for Matkul"""

    def __init__(self, nama, sks, kesulitan, semester, prasyarat=None, isWajib=1, peminatan=None):

        self.nama = nama
        self.sks = sks
        self.kesulitan = kesulitan
        self.semester = semester
        self.isWajib = isWajib
        self.prasyarat = prasyarat
        self.peminatan = peminatan

    def __eq__(self, other):
        return self.nama == other

    def __lt__(self, other):
        return self.kesulitan < other.kesulitan

    def __repr__(self):
        return self.nama


def make_matkul(nama, sks, kesulitan, semester, prasyarat=None, isWajib=1, peminatan=None):
    matkul = Matkul(nama, sks, kesulitan, semester,
                    prasyarat, isWajib, peminatan)
    return matkul


domains = [make_matkul("DDP", 6, 6, 1), make_matkul("SDA", 3, 4, 3, ["DDP"]), make_matkul(
    "PPW", 3, 4, 3, ["DDP"]), make_matkul("PSD", 4, 6, 2), make_matkul("AdvPro", 4, 7, 4, ["DDP", "SDA"]),
    make_matkul("MatDas 2", 3, 8, 3, ["MatDas 1"])]
variables = [make_matkul("MatDas 1", 3, 9, 1)]


while domains:
    select = domains.pop(0)
    heapq.heapify(domains)
    prasyarat = select.prasyarat
    lulus = variables
    if prasyarat is not None:
        for y in prasyarat:
            for x in variables:
                if x == y:
                    prasyarat.remove(y)
        if len(prasyarat) < 1:
            variables.append(select.nama)
        else:
            domains.append(select)
    else:
        variables.append(select)

    print("matkuls:", variables)
