<?php
	include('connection.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SICenayang - Help You to Prepare Your Future</title>

    <meta name="keywords" content="">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,500,700,800' rel='stylesheet' type='text/css'>

    <!-- Bootstrap and Font Awesome css -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Css animations  -->
    <link href="css/animate.css" rel="stylesheet">

    <!-- Theme stylesheet, if possible do not edit this stylesheet -->
    <link href="css/style.default.css" rel="stylesheet" id="theme-stylesheet">

    <!-- Custom stylesheet - for your changes -->
    <link href="css/custom.css" rel="stylesheet">

    <!-- Responsivity for older IE -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

    <!-- Favicon and apple touch icons-->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="img/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="img/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="img/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="img/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="img/apple-touch-icon-152x152.png" />

</head>

<body>
    <div id="all">
	<div id="heading-breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h1>LAMAN PENGISIAN MATA KULIAH</h1>
                    </div>
                    <div class="col-md-5">
                        <ul class="breadcrumb">
                            <li><a href="index.html">Home</a>
                            </li>
                            <li>LAMAN PENGISIAN MATA KULIAH</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div id="content">
            <div class="container" id="contact">
                <section>
                    <div class="row">
                        <div class="col-md-12">
                            <section>
                                <p class="lead">Rancanglah masa depan perkuliahan kalian dengan sebaik mungkin agar setiap cita-cita yang kalian impikan dapat tercapai dengan baik!
								Dengan Sistem Informasi Cenayang, masa depan anda akan terancang dengan sebaik mungkin! Prepare the best for the future!</p>
                            </section>
                        </div>
                    </div>
                </section>

                <section>
                    <div class="row text-center">
                        <div class="col-md-12">
                            <div class="heading">
                                <h2>ISI MATA KULIAH</h2>
								<p> </p>
								<p> Isi daftar mata kuliah yang telah diambil pada semester-semester yang sudah kalian jalani :</p>
                            </div>
                        </div>
						<div class="table-responsive">
				<br>
				
				
			</div>
				<div>
				<table class='table'>
					<thead> <tr> <th>Nama Matakuliah</th> <th>Kode Mata kuliah</th> </tr> </thead>
					<tbody>
						<?php
							$connect = connectDB();
							$query = "SELECT nama, kode FROM mata_kuliah order by nama ASC";
							$result1 = mysqli_query($connect, $query);
							while ($row = mysqli_fetch_row($result1)) {
								echo "<tr>";
								foreach($row as $key => $value) {
									echo "<td>$value</td>";
								}
								echo "</tr>";
							}
						?>
					</tbody>
					
				</table>
				</div>
				<form action="contact.php" method="post">
						<div id="duplicater0">
							<div class="col-md-12">
								<div class="col-md-6">
										<div class="form-group">
											<label for="nama-matakuliah"> Kode Mata Kuliah : </label>
											<input type="text" class="form-control" name="nama-matkul[]" title="Masukkan kode mata kuliah" required>
											</select>
										</div>
								</div>
								<div class="col-md-6">
										<div class="form-group">
											<label for="nama-semester">Semester :</label>
											<br>
											<select class="form-control" name="nama-semester[]" id="insert-semester" required>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
											<option value="6">6</option>
											<option value="7">7</option>
											<option value="8">8</option>
											<option value="9">9</option>
											<option value="10">10</option>
											</select>
										</div>
								</div>
							</div>
						</div>
						<div id="TambahSub">
						</div>
                        <div class="form-group">
                            <button class="btn btn-template-primary btn-lg btn-block" onclick="duplicate('TambahSub')">Tambah Matakuliah</button>
                        </div>  
                        <input type="hidden" id="insert-command" name="command" value="insert">
						<button type="submit" class="btn btn-template-main btn-lg btn-block">Submit</button>
						<input name="insertMatkul" type="hidden" value="insertMatkul"/>
                    </div>
				</form>
				</section>


            </div>
        </div>
		
<!-- FOOTERRRRRRRRRRRRRRRRR -->
        <div id="get-it">
            <div class="container">
                <div class="col-md-8 col-sm-12">
                    <h3>MAKE YOU EASIER TO PREPARE YOUR FUTURE!</h3>
                </div>
                <div class="col-md-4 col-sm-12">
                    <a href="#" class="btn btn-template-transparent-primary">LET'S PREPARE</a>
                </div>
            </div>
        </div>

        <div id="copyright">
            <div class="container">
                <div class="col-md-12">
                    <p class="pull-left">&copy; 2017. Fakultas Ilmu Komputer / ZAM ZAM Cerdas</p>
                    <p class="pull-right">Template by <a href="https://bootstrapious.com">Bootstrapious</a> & <a href="https://remoteplease.com">Remote Please</a>
                         <!-- Not removing these links is part of the license conditions of the template. Thanks for understanding :) If you want to use the template without the attribution links, you can do so after supporting further themes development at https://bootstrapious.com/donate  -->
                    </p>
                </div>
            </div>
        </div>
<!-- END OF FOOTER -->
    </div>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')
    </script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

    <script src="js/jquery.cookie.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/jquery.parallax-1.1.3.js"></script>
    <script src="js/front.js"></script>
	<script>
	var i = 0;
	function duplicate(div){
		var newdiv = document.createElement('div');
		var original = document.getElementById('duplicater' + i);
		var clone = original.cloneNode(true);
		clone.id = "duplicater" + ++i;
		document.getElementById(div).appendChild(clone);
	}
	
	</script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
    <script src="js/gmaps.js"></script>
    <script src="js/gmaps.init.js"></script>

</body>

</html>