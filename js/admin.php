<?php include('connection.php'); 
	session_start();
	if (!isset($_SESSION['login_email'])) {
			header("Location: login.php");
		}
 ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Universal - All In 1 Template</title>

    <meta name="keywords" content="">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,500,700,800' rel='stylesheet' type='text/css'>

    <!-- Bootstrap and Font Awesome css -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Css animations  -->
    <link href="css/animate.css" rel="stylesheet">

    <!-- Theme stylesheet, if possible do not edit this stylesheet -->
    <link href="css/style.default.css" rel="stylesheet" id="theme-stylesheet">

    <!-- Custom stylesheet - for your changes -->
    <link href="css/custom.css" rel="stylesheet">

    <!-- Favicon and apple touch icons-->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="img/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="img/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="img/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="img/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="img/apple-touch-icon-152x152.png" />
    <!-- owl carousel css -->

    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">
</head>

<body>
    <div id="all">

        <header>
            <!-- *** NAVBAR ***
    _________________________________________________________ -->

            <div class="navbar-affixed-top" data-spy="affix" data-offset-top="200">

                <div class="navbar navbar-default yamm" role="navigation" id="navbar">

                    <div class="container">
                        <div class="navbar-header">

                            <a class="navbar-brand home" href="admin.php">
                                <img src="img/toker4.png" alt="<?php echo $_SESSION['login_user']; ?>" class="hidden-xs hidden-sm">
                                <img src="img/logo-small.png" alt="Universal logo" class="visible-xs visible-sm"><span class="sr-only">Universal - go to homepage</span>
                            </a>
                            <div class="navbar-buttons">
                                <button type="button" class="navbar-toggle btn-template-main" data-toggle="collapse" data-target="#navigation">
                                    <span class="sr-only">Toggle navigation</span>
                                    <i class="fa fa-align-justify"></i>
                                </button>
                            </div>
                        </div>
                        <!--/.navbar-header -->

                        <div class="navbar-collapse collapse" id="navigation">

                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a data-toggle="modal" data-target="#insertKategori">Membuat Kategori</a>
                                </li>
                                <li>
									<a data-toggle="modal" data-target="#insertJasaKirim">Membuat Jasa Kirim </button></a>
								</li>
								<li>
									<a data-toggle="modal" data-target="#insertPromo">Membuat Promo</a>
								</li>
								<!-- <li class="dropdown">
                                    <a href="javascript: void(0)" class="dropdown-toggle" data-toggle="dropdown">Menambah Produk <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li>  <a href="#" data-toggle="modal" data-target="#insertProduk">Shipped Produk</a>
                                        </li>
                                        <li><a href="#" data-toggle="modal" data-target="#insertProdukPulsa">Produk Pulsa</a>
                                        </li>
                                    </ul>
                                </li> -->
                                <li>
                                    <a data-toggle="modal" data-target="#insertProdukPulsa">Membuat Produk Pulsa</a>
                                </li>
                            </ul>

                        </div>
                      
                    </div>


                </div>
                <!-- /#navbar -->

            </div>

            <!-- *** NAVBAR END *** -->

        </header>

        <section>
            <!-- *** HOMEPAGE CAROUSEL ***
 _________________________________________________________ -->

            <div class="home-carousel">

                <div class="dark-mask"></div>

                <div class="container">
                    <div class="homepage owl-carousel">
                        <div class="item">
                            <div class="row">
                                <div class="col-sm-5 right">
                                    <p>
                                        <img src="img/logo.png" alt="">
                                    </p>
                                    <h1>MAU KEREN? BELANJA DI TOKOKEREN!</h1>
                                    <p>Segala hal bisa kamu dapatkan disini
                                        <br />Harga? Jangan ditanya!</p>
                                </div>
                                <div class="col-sm-7">
                                    <img class="img-responsive" src="img/template-homepage.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">

                                <div class="col-sm-7 text-center">
                                    <img class="img-responsive" src="img/template-mac.png" alt="">
                                </div>

                                <div class="col-sm-5">
                                    <h2>46 HTML pages full of features</h2>
                                    <ul class="list-style-none">
                                        <li>Sliders and carousels</li>
                                        <li>4 Header variations</li>
                                        <li>Google maps, Forms, Megamenu, CSS3 Animations and much more</li>
                                        <li>+ 11 extra pages showing template features</li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-sm-5 right">
                                    <h1>Design</h1>
                                    <ul class="list-style-none">
                                        <li>Clean and elegant design</li>
                                        <li>Full width and boxed mode</li>
                                        <li>Easily readable Roboto font and awesome icons</li>
                                        <li>7 preprepared colour variations</li>
                                    </ul>
                                </div>
                                <div class="col-sm-7">
                                    <img class="img-responsive" src="img/test3.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-sm-7">
                                    <img class="img-responsive" src="img/test2.png" alt="">
                                </div>
                                <div class="col-sm-5">
                                    <h1>Easy to customize</h1>
                                    <ul class="list-style-none">
                                        <li>7 preprepared colour variations.</li>
                                        <li>Easily to change fonts</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.project owl-slider -->
                </div>
            </div>

            <!-- *** HOMEPAGE CAROUSEL END *** -->
        </section>

        <section class="bar background-white">
            <div class="container">
                <div class="col-md-12">


                    <div class="row">
                        <div class="col-md-4">
                            <div class="box-simple">
                                <div class="icon">
                                    <i class="fa fa-desktop"></i>
                                </div>
                                <h3>Webdesign</h3>
                                <p>Fifth abundantly made Give sixth hath. Cattle creature i be don't them behold green moved fowl Moved life us beast good yielding. Have bring.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="box-simple">
                                <div class="icon">
                                    <i class="fa fa-print"></i>
                                </div>
                                <h3>Print</h3>
                                <p>Advantage old had otherwise sincerity dependent additions. It in adapted natural hastily is justice. Six draw you him full not mean evil. Prepare garrets it expense windows shewing do an.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="box-simple">
                                <div class="icon">
                                    <i class="fa fa-globe"></i>
                                </div>
                                <h3>SEO and SEM</h3>
                                <p>Am terminated it excellence invitation projection as. She graceful shy believed distance use nay. Lively is people so basket ladies window expect.</p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="box-simple">
                                <div class="icon">
                                    <i class="fa fa-lightbulb-o"></i>
                                </div>
                                <h3>Consulting</h3>
                                <p>Fifth abundantly made Give sixth hath. Cattle creature i be don't them behold green moved fowl Moved life us beast good yielding. Have bring.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="box-simple">
                                <div class="icon">
                                    <i class="fa fa-envelope-o"></i>
                                </div>
                                <h3>Email Marketing</h3>
                                <p>Advantage old had otherwise sincerity dependent additions. It in adapted natural hastily is justice. Six draw you him full not mean evil. Prepare garrets it expense windows shewing do an.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="box-simple">
                                <div class="icon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <h3>UX</h3>
                                <p>Am terminated it excellence invitation projection as. She graceful shy believed distance use nay. Lively is people so basket ladies window expect.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="bar background-pentagon no-mb">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading text-center">
                            <h2>MEET THE TEAM</h2>
                        </div>

                        <p class="lead text-center">Kami sangat senang mengerjakan tugas basdat karena ilmu yang didapatkan banyak! Doakan kami sukses!</p>


                        <!-- *** TESTIMONIALS CAROUSEL ***
 _________________________________________________________ -->

                       <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <div class="team-member" data-animate="fadeInDown">
                                <div class="image">
                                    <a href="#">
                                        <img src="img/person-1.jpg" alt="" class="img-responsive img-circle">
                                    </a>
                                </div>
                                <h3><a href="#">AISYAH HUSNA</a></h3>
                                <p class="role">Mahasiswi</p>
                            </div>
                            <!-- /.team-member -->
                        </div>
                        <div class="col-md-3 col-sm-3" data-animate="fadeInDown">
                            <div class="team-member">
                                <div class="image">
                                    <a href="#">
                                        <img src="img/person-2.jpg" alt="" class="img-responsive img-circle">
                                    </a>
                                </div>
                                <h3><a href="#">MUNADIA RAHMA</a></h3>
                                <p class="role">Mahasiswi</p>
                            </div>
                            <!-- /.team-member -->
                        </div>
                        <div class="col-md-3 col-sm-3" data-animate="fadeInDown">
                            <div class="team-member">
                                <div class="image">
                                    <a href="#">
                                        <img src="img/person-3.png" alt="" class="img-responsive img-circle">
                                    </a>
                                </div>
                                <h3><a href="#">RATNA NUR HAYATI</a></h3>
                                <p class="role">Mahasiswi</p>
                            </div>
                            <!-- /.team-member -->
                        </div>
                        <div class="col-md-3 col-sm-3" data-animate="fadeInDown">
                            <div class="team-member">
                                <div class="image">
                                    <a href="#">
                                        <img src="img/person-4.jpg" alt="" class="img-responsive img-circle">
                                    </a>
                                </div>
                                <h3><a href="#">ZAHRA A. UMMAH</a></h3>
                                <p class="role">Mahasiswi</p>
                            </div>
                            <!-- /.team-member -->
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- /.bar -->

        <section class="bar background-image-fixed-2 no-mb color-white text-center">
            <div class="dark-mask"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="icon icon-lg"><i class="fa fa-file-code-o"></i>
                        </div>
                        <h3 class="text-uppercase">Do you want to see more?</h3>
                        <p class="lead">We have prepared for you more than 40 different HTML pages, including 5 variations of homepage.</p>
                        <p class="text-center">
                            <a href="#" class="btn btn-template-transparent-black btn-lg">Check other homepage</a>
                        </p>
                    </div>

                </div>
            </div>
        </section>

        <section class="bar background-gray no-mb">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading text-center">
                            <h2>Our clients</h2>
                        </div>

                        <ul class="owl-carousel customers">
                            <li class="item">
                                <img src="img/f1.png" alt="" class="img-responsive">
                            </li>
                            <li class="item">
                                <img src="img/f2.png" alt="" class="img-responsive">
                            </li>
                            <li class="item">
                                <img src="img/f3.png" alt="" class="img-responsive">
                            </li>
                            <li class="item">
                                <img src="img/f4.png" alt="" class="img-responsive">
                            </li>
                            <li class="item">
                                <img src="img/f5.png" alt="" class="img-responsive">
                            </li>
                            <li class="item">
                                <img src="img/f6.png" alt="" class="img-responsive">
                            </li>
                        </ul>
                        <!-- /.owl-carousel -->
                    </div>

                </div>
            </div>
        </section>
		
		<div class="modal fade" id="insertKategori" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h2 class="modal-title" id="insertModalLabel">Membuat Kategori dan Subkategori</h2>
                    </div>
                        <div class="modal-body">
                            <form action="" method="post">
                                <div class="form-group">
                                    <label for="kode-kategori">Kode Kategori :</label>
                                    <input type="text" class="form-control" id="insert-kode-kategori" name="kode-kategori" placeholder="Masukan deskripsi promo" required>
                                </div>
                                <div class="form-group">
                                    <label for="nama-kategori">Nama Kategori : </label>
                                    <input type="text" class="form-control" id="insert-nama-kategori" name="nama-kategori" placeholder="Masukkan periode awal promo" required>
                                </div>
                                <br>
                                
                                <div id="subkat">
                                <a style="text-align: center"> Subkategori 1 : </a>
                                <div id="duplikasi">
                                    <div class="form-group">
                                        <label for="kode-subkategori">Kode Subkategori :</label>
                                        <input type="text" class="form-control" id="insert-kode-subkategori" name="kode-kategori" placeholder="Masukkan periode akhir promo" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama-subkategori">Nama Subkategori :</label>
                                        <input type="text" class="form-control" id="insert-nama-subkategori" name="nama-subkategori" placeholder="Masukkan kode promo" required>
                                    </div>
                                </div>
                                </div>
                                
                                <div class="form-group">
                                    <button class="btn btn-template-primary btn-lg btn-block" onclick="duplicate()">Tambah Subkategori</button>
                                </div>  
                                <input type="hidden" id="insert-command" name="command" value="insert">
                                <button type="submit" class="btn btn-template-main btn-lg btn-block">Submit</button>
                            </form>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="modal fade" id="insertProdukPulsa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                     <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h2 class="modal-title" id="insertModalLabel">Menambah Produk Pulsa</h2>
                                </div>
                                    <div class="modal-body">
                                <form action="connection.php" method="post">
                                            <input type="hidden" name="command" value="insertProdukPulsa">
                                <div class="form-group">
                                    <label for="kode-produk-pulsa">Kode_produk</label>
                                    <input type="text" class="form-control" id="insert-kode-produk-pulsa" name="kode-produk" placeholder="Masukkan kode produk" required>
                                </div>
                                <div class="form-group">
                                    <label for="nama-produk-pulsa">Nama Produk</label>
                                    <input type="text" class="form-control" id="insert-produk-pulsa" name="nama-produk-pulsa" placeholder="Masukkan Nama Produk" required>
                                </div>
                                <div class="form-group">
                                    <label for="harga-pulsa">Harga</label>
                                    <input type="number" class="form-control" id="insert-harga-pulsa" name="harga-pulsa" placeholder="Masukkan Harga" required>
                                </div>
                                <div class="form-group">
                                    <label for="deskripsi-produk-pulsa">Deskripsi</label>
                                    <input type="text" class="form-control" id="insert-deskripsi-produk-pulsa" name="deskripi-produk-pulsa" placeholder="Masukkan Deskripsi" required>
                                </div>
                                <div class="form-group">
                                    <label for="nominal-produk-pulsa">Nominal</label>
                                    <input type="number" class="form-control" id="insert-nominal-produk-pulsa" name="nominal-produk-pulsa" placeholder="Masukkan Nominal" required>
                                </div> 
                             
                                <button type="submit" class="btn btn-template-main btn-lg btn-block">Submit</button>
                                        </form>
                                    </div>
                            </div>
                     </div>
            </div>
		

		
		<div class="modal fade" id="insertJasaKirim" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h2 class="modal-title" id="insertModalLabel">Membuat Jasa Kirim</h2>
                    </div>
                        <div class="modal-body">
                            <form action="" method="post">
                                <div class="form-group">
                                    <label for="nama-jasa-kirim">Nama</label>
                                    <input type="text" class="form-control" id="insert-nama-jasa-kirim" name="nama-jasa-kirim" placeholder="Masukkan nama jasa kirim" required>
                                </div>
                                <div class="form-group">
                                    <label for="lama-kirim">Lama Kirim </label>
                                    <input type="text" class="form-control" id="insert-lama-kirim" name="lama-kirim" placeholder="Masukkan lama pengiriman" required>
                                </div>
                                <div class="form-group">
                                    <label for="tarif">Tarif</label>
                                    <input type="text" class="form-control" id="insert-tarif" name="tarif" placeholder="Masukkan tarif pengiriman" required>
                                </div>
                                <input type="hidden" id="insert-command" name="command" value="insert">
                                <button type="submit" class="btn btn-template-main btn-lg btn-block">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
			<div class="modal fade" id="insertPromo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h2 class="modal-title" id="insertModalLabel">Membuat Promo</h2>
                    </div>
                        <div class="modal-body">
                            <form action="" method="post">
                                <div class="form-group">
                                    <label for="deskripsi-promo">Deskripsi</label>
                                    <input type="text" class="form-control" id="insert-deskripsi-promo" name="deskripsi-promo" placeholder="Masukan deskripsi promo" required>
                                </div>
                                <div class="form-group">
                                    <label for="periode-awal">Periode Awal </label>
                                    <input type="date" class="form-control" id="insert-periode-awal" name="periode-awal" placeholder="Masukkan periode awal promo" required>
                                </div>
                                <div class="form-group">
                                    <label for="periode-akhir">Periode Akhir</label>
                                    <input type="date" class="form-control" id="insert-periode-akhir" name="periode-akhir" placeholder="Masukkan periode akhir promo" required>
                                </div>
                                <div class="form-group">
                                    <label for="kode-promo">Kode Promo</label>
                                    <input type="text" class="form-control" id="insert-kode-promo" name="kode-promo" placeholder="Masukkan kode promo" required>
                                </div>
                                <div class="form-group">
                                    <label for="kategori-promo">Kategori</label>
                                    <select class="form-control">
                                        <option>Kategori 1</option>
                                        <option>Kategori 2</option>
                                      </select>
                                    
                                </div>
                                <div class="form-group">
                                    <label for="subkategori-promo">Sub Kategori</label>
                                   <select class="form-control">
                                        <option>Sub Kategori 1</option>
                                        <option>Sub Kategori 2</option>
                                      </select>
                                </div>
                                <input type="hidden" id="insert-command" name="command" value="insert">
                                <button type="submit" class="btn btn-template-main btn-lg btn-block">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- *** GET IT ***
_________________________________________________________ -->

        <div id="get-it">
            <div class="container">
                <div class="col-md-8 col-sm-12">
                    <h3>MAU BELANJA YANG KEREN-KEREN? DI TOKOKEREN AJA!</h3>
                </div>
                <div class="col-md-4 col-sm-12">
                    <a href="#" class="btn btn-template-transparent-primary">Shopping Now!</a>
                </div>
            </div>
        </div>

    <!-- #### JAVASCRIPT FILES ### -->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')
    </script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	<script>
	var count = 1;
    var original = document.getElementById('duplikasi'); 
    var original2 = document.getElementById('subkat');
    
    
    function duplicate() {
        var node1 = document.createElement("A");
        count = count+1;
        var node2 = document.createTextNode("Subkategori " + count + ":");
        node1.appendChild(node2);
        
        var clone = original.cloneNode(true);
        original.parentNode.appendChild(node1);
        original.parentNode.appendChild(clone);
    } 
	
	function duplicate2() {
        var original = document.getElementById("duplikasi2");
        var clone = original.cloneNode(true);
        original.appendChild(clone);
    } 

	function GoToUlasan() {
		window.location = "ulasan.html"
	}

        // Close the dropdown menu if the user clicks outside of it
    window.onclick = function(event) {
          if (!event.target.matches('.dropbtn')) {

            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
              var openDropdown = dropdowns[i];
              if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
              }
            }
          }
    }
	
	</script>
    <script src="js/jquery.cookie.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/jquery.parallax-1.1.3.js"></script>
    <script src="js/front.js"></script>

    

    <!-- owl carousel -->
    <script src="js/owl.carousel.min.js"></script>



</body>

</html>