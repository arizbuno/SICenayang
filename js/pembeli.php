<?php
	include('connection.php'); 
	
	if ($_SESSION['role'] !== 'pembeli') {
			header("Location: 404.html");
	}
	if (!isset($_SESSION['login_email'])) {
		header("Location: login.php");
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Universal - All In 1 Template</title>

    <meta name="keywords" content="">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,500,700,800' rel='stylesheet' type='text/css'>

    <!-- Bootstrap and Font Awesome css -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Css animations  -->
    <link href="css/animate.css" rel="stylesheet">

    <!-- Theme stylesheet, if possible do not edit this stylesheet -->
    <link href="css/style.default.css" rel="stylesheet" id="theme-stylesheet">

    <!-- Custom stylesheet - for your changes -->
    <link href="css/custom.css" rel="stylesheet">

    <!-- Favicon and apple touch icons-->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="img/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="img/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="img/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="img/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="img/apple-touch-icon-152x152.png" />
    <!-- owl carousel css -->

    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">
</head>

<body>

    <div id="all">

        <header>

           
            <!-- *** NAVBAR ***
    _________________________________________________________ -->

            <div class="navbar-affixed-top" data-spy="affix" data-offset-top="200">

                <div class="navbar navbar-default yamm" role="navigation" id="navbar">

                    <div class="container">
                        <div class="navbar-header">

                            <a class="navbar-brand home" href="pembeli.php">
                                <img src="img/TKlogo.png" alt="Universal logo" class="hidden-xs hidden-sm">
                                <img src="img/TKlogo2.png" alt="Universal logo" class="visible-xs visible-sm"><span class="sr-only">Universal - go to homepage</span>
                            </a>
                            <div class="navbar-buttons">
                                <button type="button" class="navbar-toggle btn-template-main" data-toggle="collapse" data-target="#navigation">
                                    <span class="sr-only">Toggle navigation</span>
                                    <i class="fa fa-align-justify"></i>
                                </button>
                            </div>
                        </div>
                        <!--/.navbar-header -->

                        <div class="navbar-collapse collapse" id="navigation">

                            <ul class="nav navbar-nav navbar-right">
								<li class="dropdown">
                                    <a href="javascript: void(0)" class="dropdown-toggle" data-toggle="dropdown">Melihat Transaksi<b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li> <a href="#" data-toggle="modal" data-target="#myModal">Transaki Shipped Produk</a>
                                        </li>
                                        <li> <a href="#" data-toggle="modal" data-target="#myModal1">Transaki Pulsa</a>
                                        </li>
                                    </ul>
                                </li>
								<li class="dropdown">
                                    <a href="javascript: void(0)" class="dropdown-toggle" data-toggle="dropdown">Membeli Produk<b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li> <a href="#" data-toggle="modal" data-target="#Modalku1">Daftar Shipped Produk</a>
                                        </li>
                                        <li> <a href="#" data-toggle="modal" data-target="#Modalku">Daftar Produk Pulsa</a>
                                        </li>
                                    </ul>
                                </li>
								<li>
                                   <a data-toggle="modal" data-target="#insertToko"> Membuka Toko</a>
                                </li>
								<li>
									<a class="btn btn-info" href="logout.php">Logout</a>
								</li>
                            </ul>

                        </div>
                    </div>


                </div>
                <!-- /#navbar -->

            </div>

            <!-- *** NAVBAR END *** -->

        </header>

        <!-- *** LOGIN MODAL END *** -->

        <section>
            <!-- *** HOMEPAGE CAROUSEL ***
 _________________________________________________________ -->

            <div class="home-carousel">

                <div class="dark-mask"></div>

                <div class="container">
                    <div class="homepage owl-carousel">
                        <div class="item">
                            <div class="row">
                                <div class="col-sm-5 right">
                                    <p>
                                        <img src="img/TKlogo.png" alt="">
                                    </p>
                                    <h1>MAU KEREN? BELANJA DI TOKOKEREN!</h1>
                                    <p>Segala hal bisa kamu dapatkan disini
                                        <br />Harga? Jangan ditanya!</p>
                                </div>
                                <div class="col-sm-7">
                                    <img class="img-responsive" src="img/template-homepage.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">

                                <div class="col-sm-7 text-center">
                                    <img class="img-responsive" src="img/template-mac.png" alt="">
                                </div>

                                <div class="col-sm-5">
                                    <h2>46 HTML pages full of features</h2>
                                    <ul class="list-style-none">
                                        <li>Sliders and carousels</li>
                                        <li>4 Header variations</li>
                                        <li>Google maps, Forms, Megamenu, CSS3 Animations and much more</li>
                                        <li>+ 11 extra pages showing template features</li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-sm-5 right">
                                    <h1>Design</h1>
                                    <ul class="list-style-none">
                                        <li>Clean and elegant design</li>
                                        <li>Full width and boxed mode</li>
                                        <li>Easily readable Roboto font and awesome icons</li>
                                        <li>7 preprepared colour variations</li>
                                    </ul>
                                </div>
                                <div class="col-sm-7">
                                    <img class="img-responsive" src="img/test3.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-sm-7">
                                    <img class="img-responsive" src="img/test2.png" alt="">
                                </div>
                                <div class="col-sm-5">
                                    <h1>Easy to customize</h1>
                                    <ul class="list-style-none">
                                        <li>7 preprepared colour variations.</li>
                                        <li>Easily to change fonts</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.project owl-slider -->
                </div>
            </div>

            <!-- *** HOMEPAGE CAROUSEL END *** -->
        </section>

        <section class="bar background-white">
            <div class="container">
                <div class="col-md-12">


                    <div class="row">
                        <div class="col-md-4">
                            <div class="box-simple">
                                <div class="icon">
                                    <i class="fa fa-desktop"></i>
                                </div>
                                <h3>Webdesign</h3>
                                <p>Fifth abundantly made Give sixth hath. Cattle creature i be don't them behold green moved fowl Moved life us beast good yielding. Have bring.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="box-simple">
                                <div class="icon">
                                    <i class="fa fa-print"></i>
                                </div>
                                <h3>Print</h3>
                                <p>Advantage old had otherwise sincerity dependent additions. It in adapted natural hastily is justice. Six draw you him full not mean evil. Prepare garrets it expense windows shewing do an.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="box-simple">
                                <div class="icon">
                                    <i class="fa fa-globe"></i>
                                </div>
                                <h3>SEO and SEM</h3>
                                <p>Am terminated it excellence invitation projection as. She graceful shy believed distance use nay. Lively is people so basket ladies window expect.</p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="box-simple">
                                <div class="icon">
                                    <i class="fa fa-lightbulb-o"></i>
                                </div>
                                <h3>Consulting</h3>
                                <p>Fifth abundantly made Give sixth hath. Cattle creature i be don't them behold green moved fowl Moved life us beast good yielding. Have bring.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="box-simple">
                                <div class="icon">
                                    <i class="fa fa-envelope-o"></i>
                                </div>
                                <h3>Email Marketing</h3>
                                <p>Advantage old had otherwise sincerity dependent additions. It in adapted natural hastily is justice. Six draw you him full not mean evil. Prepare garrets it expense windows shewing do an.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="box-simple">
                                <div class="icon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <h3>UX</h3>
                                <p>Am terminated it excellence invitation projection as. She graceful shy believed distance use nay. Lively is people so basket ladies window expect.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="bar background-pentagon no-mb">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading text-center">
                            <h2>MEET THE TEAM</h2>
                        </div>

                        <p class="lead text-center">Kami sangat senang mengerjakan tugas basdat karena ilmu yang didapatkan banyak! Doakan kami sukses! Aamiin :)</p>


                        <!-- *** TESTIMONIALS CAROUSEL ***
 _________________________________________________________ -->

                       <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <div class="team-member" data-animate="fadeInDown">
                                <div class="image">
                                    <a href="#">
                                        <img src="img/person-1.jpg" alt="" class="img-responsive img-circle">
                                    </a>
                                </div>
                                <h3><a href="#">AISYAH HUSNA</a></h3>
                                <p class="role">Mahasiswi</p>
                            </div>
                            <!-- /.team-member -->
                        </div>
                        <div class="col-md-3 col-sm-3" data-animate="fadeInDown">
                            <div class="team-member">
                                <div class="image">
                                    <a href="#">
                                        <img src="img/person-2.jpg" alt="" class="img-responsive img-circle">
                                    </a>
                                </div>
                                <h3><a href="#">MUNADIA RAHMA</a></h3>
                                <p class="role">Mahasiswi</p>
                            </div>
                            <!-- /.team-member -->
                        </div>
                        <div class="col-md-3 col-sm-3" data-animate="fadeInDown">
                            <div class="team-member">
                                <div class="image">
                                    <a href="#">
                                        <img src="img/person-3.png" alt="" class="img-responsive img-circle">
                                    </a>
                                </div>
                                <h3><a href="#">RATNA NUR HAYATI</a></h3>
                                <p class="role">Mahasiswi</p>
                            </div>
                            <!-- /.team-member -->
                        </div>
                        <div class="col-md-3 col-sm-3" data-animate="fadeInDown">
                            <div class="team-member">
                                <div class="image">
                                    <a href="#">
                                        <img src="img/person-4.jpg" alt="" class="img-responsive img-circle">
                                    </a>
                                </div>
                                <h3><a href="#">ZAHRA A. UMMAH</a></h3>
                                <p class="role">Mahasiswi</p>
                            </div>
                            <!-- /.team-member -->
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- /.bar -->

        <section class="bar background-image-fixed-2 no-mb color-white text-center">
            <div class="dark-mask"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="icon icon-lg"><i class="fa fa-file-code-o"></i>
                        </div>
                        <h3 class="text-uppercase">Do you want to see more?</h3>
                        <p class="lead">We have prepared for you more than 40 different HTML pages, including 5 variations of homepage.</p>
                        <p class="text-center">
                            <a href="#" class="btn btn-template-transparent-black btn-lg">Check other homepage</a>
                        </p>
                    </div>

                </div>
            </div>
        </section>

        <section class="bar background-gray no-mb">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading text-center">
                            <h2>Our clients</h2>
                        </div>

                        <ul class="owl-carousel customers">
                            <li class="item">
                                <img src="img/f1.png" alt="" class="img-responsive">
                            </li>
                            <li class="item">
                                <img src="img/f2.png" alt="" class="img-responsive">
                            </li>
                            <li class="item">
                                <img src="img/f3.png" alt="" class="img-responsive">
                            </li>
                            <li class="item">
                                <img src="img/f4.png" alt="" class="img-responsive">
                            </li>
                            <li class="item">
                                <img src="img/f5.png" alt="" class="img-responsive">
                            </li>
                            <li class="item">
                                <img src="img/f6.png" alt="" class="img-responsive">
                            </li>
                        </ul>
                        <!-- /.owl-carousel -->
                    </div>

                </div>
            </div>
        </section>
		
		
		<div class="modal fade" id="myModal1" role="dialog">
			<div class="modal-dialog modal-lg">
			  <div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title">Transaksi Pulsa</h4>
				</div>
				<div class="modal-body">
				  <p>Daftar Transaksi Pulsa</p>
						<table class="table table-hover">
							  <thead>
								<tr>
								  <th>No.Invoice</th>
								  <th>Nama Produk</th>
								  <th>Tanggal</th>
								  <th>Status</th>
								  <th>Total Bayar</th>
								  <th>Nominal</th>
								  <th>Nomor</th>
								  <th>ULASAN</th>
								</tr>
							  </thead>
							  <tbody>
								<tr>
								  <td>1</td>
								  <td>dummy</td>
								  <td>dummy</td>
								  <td>dummy</td>
								  <td>dummy</td>
								  <td>dummy</td>
								  <td>dummy</td>
								  <td><button type="button" onclick="GoToUlasan()">ULAS</button></td>
								</tr>
								<tr>
								  <td>2</td>
								  <td>dummy</td>
								  <td>dummy</td>
								  <td>dummy</td>
								  <td>dummy</td>
								  <td>dummy</td>
								  <td>dummy</td>
								  <td><button type="button" onclick="GoToUlasan()" >ULAS</button></td>
								</tr>   
							  </tbody>
							</table>
							<div class="col-sm-12 ">
								<ul class="pagination pull-right">
								  <li><a href="#">«</a></li>
								  <li><a href="#">1</a></li>
								  <li><a href="#">2</a></li>
								  <li><a href="#">3</a></li>
								  <li><a href="#">4</a></li>
								  <li><a href="#">5</a></li>
								  <li><a href="#">»</a></li>
								</ul>       
							</div>
				</div>
				<div class="modal-footer">
				  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			  </div>
			</div>
		  </div>
		  
		<div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog modal-lg">
			  <div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title">Transaksi Shipped</h4>
				</div>
				<div class="modal-body">
				  <p>Daftar Transaksi Shipped</p>
						 <table class="table table-hover">
							  <thead>
								<tr>
								  <th>No.Invoice</th>
								  <th>Nama Toko</th>
								  <th>Tanggal</th>
								  <th>Status</th>
								  <th>Total Bayar</th>
								  <th>Alamat Kirim</th>
								  <th>Biaya Kirim</th>
								  <th>Nomor Resi</th>
								  <th>Jasa Kirim</th>
								  <th>ULASAN</th>
								</tr>
							  </thead>
							  <tbody>
								<tr>
								  <td>1</td>
								  <td>John's Sport</td>
								  <td>Dell</td>
								  <td>dummy</td>
								  <td>dummy</td>
								  <td>dummy</td>
								  <td>dummy</td>
								  <td>dummy</td>
								  <td>dummy</td>
								  <td><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#coba">Daftar Produk</button>
										<div class="modal fade" id="coba" role="dialog">
											<div class="modal-dialog modal-lg">
											  <div class="modal-content">
												<div class="modal-header">
												  <button type="button" class="close" data-dismiss="modal">&times;</button>
												  <h4 class="modal-title">Daftar Produk</h4>
												</div>
												<div class="modal-body">
												  <p>Daftar Produk Dibeli</p>
														<table class="table table-hover">
															  <thead>
																<tr>
																  
																  <th>Nama Produk</th>
																  <th>Tanggal</th>
																  <th>Status</th>
																  <th>Total Bayar</th>
																  <th>Nominal</th>
																
																  <th>ULASAN</th>
																</tr>
															  </thead>
															  <tbody>
																<tr>
																  <td>dummy</td>
																  <td>dummy</td>
																  <td>dummy</td>
																  <td>dummy</td>
																  <td>dummy</td>
																  <td><button type="button" onclick="GoToUlasan()">ULAS</button></td>
																</tr>
															  </tbody>
															</table>
															<div class="col-sm-12 ">
																<ul class="pagination pull-right">
																  <li><a href="#">«</a></li>
																  <li><a href="#">1</a></li>
																  <li><a href="#">2</a></li>
																  <li><a href="#">3</a></li>
																  <li><a href="#">4</a></li>
																  <li><a href="#">5</a></li>
																  <li><a href="#">»</a></li>
																</ul>       
															</div>
															
															
													</div>
											  </div>
											</div>
										  </div>
										  </td>  
		  
		  
								</tr>
									<tr>
									  <td>2</td>
									  <td>Dell 18.5 Inch Monitor</td>
									  <td>Dell</td>
									  <td>dummy</td>
									  <td>dummy</td>
									  <td>dummy</td>
									  <td>dummy</td>
									  <td>dummy</td>
									  <td>dummy</td>
									  <td><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#coba">Daftar Produk</button>
										<div class="modal fade" id="" role="dialog">
											<div class="modal-dialog modal-lg">
											  <div class="modal-content">
												<div class="modal-header">
												  <button type="button" class="close" data-dismiss="modal">&times;</button>
												  <h4 class="modal-title">Transaksi Pulsa</h4>
												</div>
												<div class="modal-body">
												  <p>Daftar Transaksi Pulsa</p>
														<table class="table table-hover">
															  <thead>
																<tr>
																  <th>No.Invoice</th>
																  <th>Nama Produk</th>
																  <th>Tanggal</th>
																  <th>Status</th>
																  <th>Total Bayar</th>
																  <th>Nominal</th>
																  <th>Nomor</th>
																  <th>ULASAN</th>
																</tr>
															  </thead>
															  <tbody>
																<tr>
																  <td>1</td>
																  <td>dummy</td>
																  <td>dummy</td>
																  <td>dummy</td>
																  <td>dummy</td>
																  <td>dummy</td>
																  <td>dummy</td>
																  <td><button type="button" onclick="GoToUlasan()">ULAS</button></td>
																</tr>
																<tr>
																  <td>2</td>
																  <td>dummy</td>
																  <td>dummy</td>
																  <td>dummy</td>
																  <td>dummy</td>
																  <td>dummy</td>
																  <td>dummy</td>
																  <td><button type="button" onclick="GoToUlasan()">ULAS</button></td>
																</tr>   
															  </tbody>
															</table>
															<div class="col-sm-12 ">
																<ul class="pagination pull-right">
																  <li><a href="#">«</a></li>
																  <li><a href="#">1</a></li>
																  <li><a href="#">2</a></li>
																  <li><a href="#">3</a></li>
																  <li><a href="#">4</a></li>
																  <li><a href="#">5</a></li>
																  <li><a href="#">»</a></li>
																</ul>       
															</div>
												</div>
												<div class="modal-footer">
												  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											  </div>
											</div>
										  </div>
										  </td>
									</tr>        
							  </tbody>
							</table>
							<div class="col-sm-12 ">
								<ul class="pagination pull-right">
								  <li><a href="#">«</a></li>
								  <li><a href="#">1</a></li>
								  <li><a href="#">2</a></li>
								  <li><a href="#">3</a></li>
								  <li><a href="#">4</a></li>
								  <li><a href="#">5</a></li>
								  <li><a href="#">»</a></li>
								</ul>       
							</div>
				</div>
				<div class="modal-footer">
				  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			  </div>
			</div>
		  </div>
		  
		  <div class="modal fade" id="insertToko" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h2 class="modal-title" id="insertModalLabel">Membuka Toko</h2>
                                </div>
                                    <div class="modal-body">
                                <form action="" method="post">
                                <div class="form-group">
                                    <label for="nama-toko">Nama</label>
                                    <input type="text" class="form-control" id="insert-namaToko" name="nama-toko" placeholder="Masukkan kode produk" required>
                                </div>
                                <div class="form-group">
                                    <label for="deskrpsi">Deskripsi</label>
                                    <input type="text" class="form-control" id="insert-deskripsiToko" name="deskripsiToko" placeholder="Masukkan Deskripsi" required>
                                </div>
                                <div class="form-group">
                                    <label for="slogan">Slogan</label>
                                    <input type="text" class="form-control" id="insert-sloganToko" name="sloganToko" placeholder="Masukkan Nama Produk" required>
                                </div>
                                <div class="form-group">
                                    <label for="lokasiToko">Lokasi</label>
                                    <input type="text" class="form-control" id="insert-lokasiToko" name="lokasiToko" placeholder="Masukkan Harga" required>
                                </div>
                                <div class="form-group">
                                            <label for="subkategori-produk">Jasa Kirim : </label>
                                            <select class="form-control" name="sub-kategori">
                                                <?php 
                                                    $con = connectDB();
                                                    $result = pg_query($con, 'SELECT * FROM jasa_kirim');

                                                    while ($row = pg_fetch_row($result)) {
                                                      echo "<option value='".$row[0]."''>".$row[0]."</option>";
                                                    }
                                                ?>
                                              </select>
                                        </div>

                                <div class="form-group">
                                    <button class="btn btn-primary btn-lg btn-block" onclick="duplicate2()">Tambah Jasa Kirim</button>
                                </div>

                                <input type="hidden" id="insert-command" name="command" value="insert">
                                <button type="submit" class="btn btn-template-main btn-lg btn-block">Submit</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
		 
		
		
			
        </div>
		
		  <div class="modal fade" id="Modalku" role="dialog">
			<div class="modal-dialog modal-lg">
			  <div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title">Daftar Produk Pulsa</h4>
				</div>
				<div class="modal-body">
				  <p>Daftar Produk Pulsa</p>
						 <table class="table table-hover">
							  <thead>
								<tr>
								  <th>Kode Produk</th>
								  <th>Nama Produk</th>
								  <th>Harga</th>
								  <th>Deskripsi</th>
								  <th>Beli</th>
								</tr>
							  </thead>
							  <tbody>
								<tr>
								  <td>1</td>
								  <td>dummy</td>
								  <td>dummy</td>
								  <td>dummy</td>
								  <td><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#coba2">Beli</button>
										<div class="modal fade" id="coba2" role="dialog">
											<div class="modal-dialog modal-lg">
											  <div class="modal-content">
												<div class="modal-header">
												  <button type="button" class="close" data-dismiss="modal">&times;</button>
												  <p>Form Membeli Produk Pulsa</p>
												</div>
												<div class="modal-body">
												  <form action="indexhome.php" method="post">
														<div class="form-group">
															<label for="kode">Kode Produk</label>
															<input type="text" class="form-control" id="insert-kode" name="kode" placeholder="Tambah Kode Prodek">
														</div>
														<div class="form-group">
															<label for="nomor">Nomor HP/Token Listrik</label>
															<input type="text" class="form-control" id="insert-nomor" name="title" placeholder="Tambah Nomor HP/Token Listrik ">
														</div>												
														<input type="hidden" id="insert-command" name="command" value="insert">
														<button type="submit" class="btn btn-template-main btn-lg btn-block"> Submit</button>
													</form>
															
															
													</div>
											  </div>
											</div>
										  </div>
										  </td> 
								</tr>
								<tr>
								  <td>2</td>
								  <td>dummy</td>
								  <td>dummy</td>
								  <td>dummy</td>
								  <td><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#coba">Beli</button>
										<div class="modal fade" id="coba" role="dialog">
											<div class="modal-dialog modal-lg">
											  <div class="modal-content">
												<div class="modal-header">
												  <button type="button" class="close" data-dismiss="modal">&times;</button>
												  <p>Form Membeli Produk Pulsa</p>
												</div>
												<div class="modal-body">
												  <form action="indexhome.php" method="post">
														<div class="form-group">
															<label for="kode">Kode Produk</label>
															<input type="text" class="form-control" id="insert-kode" name="kode" placeholder="Tambah Kode Prodek">
														</div>
														<div class="form-group">
															<label for="nomor">Nomor HP/Token Listrik</label>
															<input type="text" class="form-control" id="insert-nomor" name="title" placeholder="Tambah Nomor HP/Token Listrik ">
														</div>												
														<input type="hidden" id="insert-command" name="command" value="insert">
														<button type="submit" class="btn btn-template-main btn-lg btn-block"> Submit</button>
													</form>
															
															
													</div>
											  </div>
											</div>
										  </div>
										 </td> 
								</tr>        
							  </tbody>
							</table>
				</div>
				<div class="modal-footer">
				  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			  </div>
			</div>
		  </div>
		  
		  <div class="modal fade" id="Modalku1" role="dialog">
			<div class="modal-dialog modal-lg">
			  <div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title">Form Pilih Toko</h4>
				</div>
				<div class="modal-body">
				  <p>Form Pilih Toko</p>
					<div class="form-group">
                         <label for="nama">Nama Toko : </label>
                             <select class="form-control">
								  <option>Toko A</option>
								  <option>Toko B</option>
                              </select>                                    
                     </div>
				
				  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#submit">Submit</button>
				</div>
				
				<div class="modal-footer">
				  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			  </div>
			</div>
		  </div>
		
		<div class="modal fade" id="submit" role="dialog">
					<div class="modal-dialog modal-lg">
					  <div class="modal-content">
						<div class="modal-header">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">Daftar Shipped Produk</h4>
						</div>
						<div class="modal-body">
						  <p>Daftar Shipped Produk</p>
						  <div class="form-group">
							<label for="nama">Kategori: </label>
                             <select class="form-control">
								  <option>Kategori A</option>
								  <option>Kategori B</option>
                              </select>                                    
							</div>
							
							<div class="form-group">
							<label for="nama">Sub Kategori: </label>
                             <select class="form-control">
								  <option>Sub Kategori A</option>
								  <option>Sub Kategori B</option>
                              </select>                                    
							</div>
						  
						  
						  
								 <table class="table table-hover">
									  <thead>
										<tr>
										  <th>Kode Produk</th>
										  <th>Nama Produk</th>
										  <th>Harga</th>								  							  
										  <th>Deskripsi</th>
										  <th>Is Asuransi</th>	
										  <th>Stok</th>
										  <th>Is Baru</th>
										  <th>Harga Grosir</th>
										  <th>Beli</th>								  
										</tr>
									  </thead>
									  <tbody>
										<tr>
										  <td>1</td>
										  <td>dummy</td>
										  <td>dummy</td>
										  <td>dummy</td>
										  <td>dummy</td>
										  <td>dummy</td>
										  <td>dummy</td>
										  <td>dummy</td>
										  <td><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#coba1">Beli</button>
											<div class="modal fade" id="coba1" role="dialog">
												<div class="modal-dialog modal-lg">
												  <div class="modal-content">
													<div class="modal-header">
													  <button type="button" class="close" data-dismiss="modal">&times;</button>
													  <p>Daftar Shipped Produk</p>
													</div>
													<div class="modal-body">
															<table class="table table-hover">
															  <thead>
																<tr>
																  
																  <th>Kode Produk</th>
																  <th>Nama Produk</th>
																  <th>Berat</th>
																  <th>Kuantitas</th>
																  <th>Harga</th>
																  <th>Sub total</th>
																</tr>
															  </thead>
															  <tbody>
																<tr>
																  <td>dummy</td>
																  <td>dummy</td>
																  <td>dummy</td>
																  <td>dummy</td>
																  <td>dummy</td>
																  <td>dummy</td>																  
																</tr>
															  </tbody>
															</table>
														<div class="col-sm-12 ">
															<ul class="pagination pull-right">
															  <li><a href="#">«</a></li>
															  <li><a href="#">1</a></li>
															  <li><a href="#">2</a></li>
															  <li><a href="#">3</a></li>
															  <li><a href="#">4</a></li>
															  <li><a href="#">5</a></li>
															  <li><a href="#">»</a></li>
															</ul>       
														</div>
													
													
													
														<div class="form-group">
														<label for="jasa">Jasa Kirim: </label>
														 <select class="form-control">
															  <option>Jasa A</option>
															  <option>Jasa B</option>
														  </select>                                    
														</div>
													  <form action="indexhome.php" method="post">
															<div class="form-group">
																<label for="alamat">Alamat Kirim</label>
																<input type="text" class="form-control" id="insert-alamat" name="kode" placeholder="Masukan Alamat">
															</div>
																											
															<input type="hidden" id="insert-command" name="command" value="insert">
															<button type="submit" class="btn btn-default">Checkout</button>
														</form>
																
																
														</div>
												  </div>
												</div>
											  </div>
										 </td>
										   
				  
				  
										</tr>
												   
									  </tbody>
									</table>
									<div class="col-sm-12 ">
										<ul class="pagination pull-right">
										  <li><a href="#">«</a></li>
										  <li><a href="#">1</a></li>
										  <li><a href="#">2</a></li>
										  <li><a href="#">3</a></li>
										  <li><a href="#">4</a></li>
										  <li><a href="#">5</a></li>
										  <li><a href="#">»</a></li>
										</ul>       
									</div>
						</div>
						<div class="modal-footer">
						  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					  </div>
					</div>
				  </div>
		
			
        </div>

        <!-- *** GET IT ***
_________________________________________________________ -->

        <div id="get-it">
            <div class="container">
                <div class="col-md-8 col-sm-12">
                    <h3>MAU BELANJA YANG KEREN-KEREN? DI TOKOKEREN AJA!</h3>
                </div>
                <div class="col-md-4 col-sm-12">
                    <a href="#" class="btn btn-template-transparent-primary">Shopping Now!</a>
                </div>
            </div>
        </div>

    <!-- #### JAVASCRIPT FILES ### -->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')
    </script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

    <script src="js/jquery.cookie.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/jquery.parallax-1.1.3.js"></script>
    <script src="js/front.js"></script>

    

    <!-- owl carousel -->
    <script src="js/owl.carousel.min.js"></script>



</body>

</html>